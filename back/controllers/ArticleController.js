
const Articles = require("../models/Articles");

class ArticleController {
    create = (req, res) => {
        const { name, description, price } = req.body;
        const { photo } = req.files;
        console.log(photo[0].mimetype);
        if (!name || !price) {
            return res.status(500).send('Need to send at least name and price !')
        }
        if (name, price) {
            // Articles.create({
            //     name: name,
            //     description: description,
            //     photo: photo,
            //     price: price
            // });
            return res.status(201).send('Article created successfuly !');
        }
    }
    fetchAll = async (req, res) => {
        const articles = await Articles.findAll();
        return res.status(201).send(articles);
    }
}

module.exports.ArticleController = ArticleController;