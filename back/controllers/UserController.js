const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require('dotenv').config();

class UserController {
    create = async (req, res) =>
    {
        const { email, password } = req.body;
        if (email, password) {
            const user = await User.findOne({ where: { email: email } });
            if (user) {
                return res.status(500).send('Already exists');
            }
            bcrypt.hash(password, 10, (err, hash) => {
                User.create({
                    email: email,
                    password: hash
                });
                return res.status(201).send('User created successfuly !');
            })
        } else {
            return res.status(500).send('All fields need to be completed !');
        }
    }
    fetchOne = async (req, res) =>
    {
        const { user } = req;
        res.status(201).send({user});
    }
    update = async (req, res) =>
    {
        const { id } = req.params;
        const { address, role, password, activation } = req.body;
        if (id) {
            const user = await User.findOne({ where: { id: id } });
            if (user === null) {
                return res.status(404).send("Not found");
            }
            if (password) {
                bcrypt.hash(password, 10, function(err, hash) {
                    user.password = hash;
                });
            }
            user.address = address;
            user.role = role;
            user.activation = activation;
            user.save();
            return res.status(201).send('User updated');
        }
    }
    delete = async (req, res) =>
    {
        const { id } = req.params;
        if (id) {
            const user = await User.findOne({ where: { id: id } });
            if (user === null) {
                return res.status(404).send('Not found');
            }
            user.destroy();
            return res.status(201).send('User deleted');
        }
    }
    login = async (req, res) =>
    {
        const { email, password } = req.body;
        if (email, password) {
            // Verify if user exists
            const user = await User.findOne({where: { email: email }});
            if (user === null) {
                return res.status(404).send("Email or password is wrong");
            } else {
                // compare passwords
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        const { address, email, id, createdAt, updatedAt, role, activation } = user
                        const userData = {
                            id,
                            email,
                            address,
                            role,
                            activation,
                            createdAt,
                            updatedAt
                        }
                        const accessToken = this.generateAccessToken(userData);
                        res.status(201).send({userData, token: accessToken});
                    } else {
                        return res.status(404).send(err);
                    }
                })
            }
        }  else {
            return res.status(500).send('All fields need to be completed !');
        }
    }
    generateAccessToken = (user) => 
    {
        return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '3600s' });
    }
}

module.exports.UserController = UserController;