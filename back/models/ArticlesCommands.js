const { DataTypes } = require('sequelize');
const sequelize = require('../database/connect');

const ArticlesCommands = sequelize.define('ArticlesCommands', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  idArticle: {
    type: DataTypes.INTEGER,
    onDelete: 'CASCADE',
    references: {
        model: 'articles',
        key: 'id'
    }
  },
  idComm: {
    type: DataTypes.INTEGER,
    onDelete: 'CASCADE',
    references: {
        model: 'commandes',
        key: 'id',
    }
  },
  quantity: {
    type: DataTypes.INTEGER,
  }
}, {
    tableName: 'articles_commands',
    associate: function(models) {
        ArticlesCommands.belongsTo(models.Commandes, { onDelete:'cascade' });
        ArticlesCommands.belongsTo(models.Articles, { onDelete: 'cascade' });
    }
});

module.exports = ArticlesCommands;