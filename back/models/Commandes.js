const { DataTypes } = require('sequelize');
const sequelize = require('../database/connect');

const Commandes = sequelize.define('Commandes', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  statut: {
    type: DataTypes.STRING
  },
  userId: {
    type: DataTypes.INTEGER,
    onDelete: 'CASCADE',
    references: {
        model: 'users',
        key: 'id',
    }
  }
}, {
    tableName: 'commandes',
    associate: function(models) {
        User.belongsTo(models.User, { onDelete:'cascade' });
    }
});

module.exports = Commandes;