const { DataTypes } = require('sequelize');
const sequelize = require('../database/connect');

const Articles = sequelize.define('Articles', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  description: {
    type: DataTypes.STRING
  },
  photo: {
    type: DataTypes.STRING,
  },
  price: {
    type: DataTypes.FLOAT,
    allowNull: false,
  }
}, {
    tableName: 'articles'
});

module.exports = Articles;