const express = require('express')
const cors = require('cors');
const app = express()
const sequelize = require('./database/connect');
const synchroAll = require('./database/migrations/synchronize');
const userRouter = require('./router/user');
const articleRouter = require('./router/article');

// testing the connection to our database
try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}
// Make migrations to db
synchroAll();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('/public'));

// Routes
app.use('/users', userRouter);
app.use('/articles', articleRouter);

app.listen(8000);