const synchroniseSequelizeArticles = require("./articles");
const synchroniseSequelizeCommandes = require("./commandes");
const synchroniseSequelizeUser = require("./users");

const synchroAll = () => {
    synchroniseSequelizeUser();
    synchroniseSequelizeCommandes();
    synchroniseSequelizeArticles();
}

module.exports = synchroAll;