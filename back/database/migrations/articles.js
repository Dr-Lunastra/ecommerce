const Articles = require("../../models/Articles")

const synchroniseSequelizeArticles = async () => {
    try {
        await Articles.sync();
    } catch(error) {
        console.error( error);
    }
}

module.exports = synchroniseSequelizeArticles;