const Commandes = require("../../models/Commandes")

const synchroniseSequelizeCommandes = async () => {
    try {
        await Commandes.sync();
    } catch(error) {
        console.error( error);
    }
}

module.exports = synchroniseSequelizeCommandes;