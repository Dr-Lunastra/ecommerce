const User = require("../../models/User")

const synchroniseSequelizeUser = async () => {
    try {
        await User.sync();
    } catch(error) {
        console.error( error);
    }
}

module.exports = synchroniseSequelizeUser;