const express = require('express');
const { ArticleController } = require('../controllers/ArticleController');
const AuthenticateToken = require('../middleware/authToken');
var multer = require('multer');
const articleRouter = express.Router();
const upload = multer();

const articleController = new ArticleController

articleRouter.post('/', [AuthenticateToken, upload.fields([{ name: 'photo', maxCount: 1 }])], articleController.create);
articleRouter.get('/', articleController.fetchAll);

module.exports = articleRouter;