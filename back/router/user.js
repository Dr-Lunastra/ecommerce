const express = require('express');
const { UserController } = require('../controllers/UserController');
const AuthenticateToken = require('../middleware/authToken');
const userRouter = express.Router();

const userController = new UserController

userRouter.get('/', AuthenticateToken,  userController.fetchOne);
userRouter.post('/', userController.create);
userRouter.post('/auth', userController.login);
userRouter.put('/:id', userController.update);
userRouter.delete('/:id', userController.delete);

module.exports = userRouter;