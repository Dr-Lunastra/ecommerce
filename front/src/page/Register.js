import { useContext, useEffect, useState } from "react";
import { validateEmail } from "../utils/validations";
import { compareStringEquality } from "../utils/compare";
import axios from "axios";
import Swal from "sweetalert2";
import { UserContext } from "../contexts/userContext";
import { useNavigate } from "react-router-dom";

const Register = () => {

        const { user } = useContext(UserContext);
        const navigate = useNavigate();

        useEffect(() => {
            if (user && user.role === 'admin') {
                navigate('/dashboard');
            }
        }, [user, navigate])

    const [ email, setEmail ] = useState('');
    const [ isEmailCorrect, setIsEmailCorrect ] = useState(false);
    const [ password, setPassword ] = useState('');
    const [ confirmPassword, setConfirmPassword ] = useState('');
    const [ isEqualPassword, setIsEqualPassword ] = useState(false);

    const handleEmail = (e) => {
        const { value } = e.target;
        setEmail(value);
        const isValidEmail = validateEmail(value);
        setIsEmailCorrect(isValidEmail);
    }

    const handleConfirmPassword = (e) => {
        const { value } = e.target
        setConfirmPassword(value);
        const isEqual = compareStringEquality(password, value);
        setIsEqualPassword(isEqual);
    }

    const submit = (e) => {
        e.preventDefault();
        if (isEmailCorrect && isEqualPassword) {
            axios.post(`${process.env.REACT_APP_API_URL}users/`, {email, password})
            .then((res) => {
                displaySwal(res.data);
            })
            .catch((err) => {
                const { data } = err.response;
                displaySwal(data, 'error');
            })
            .finally(() => {
                setEmail('');
                setPassword('');
                setConfirmPassword('');
            })
        }
    }

    const displaySwal = (text, type = 'success') => {
        Swal.fire({
            position: 'top-end',
            icon: type,
            title: text,
            showConfirmButton: false,
            timer: 1500
          })
    }

    return (
        <div className="hero min-h-screen bg-base-200">
            <div className="hero-content flex-col lg:flex-row-reverse">
                <div className="text-center lg:text-left">
                    <h1 className="text-5xl font-bold">Register now!</h1>
                    <p className="py-6">Vous savez ce qu'il vous reste à faire si vous voulez pouvoir suivre vos commandes ! ;)</p>
                </div>
                <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
                    <div className="card-body">
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Email</span>
                            </label>
                            <input type="text" placeholder="email" className="input input-bordered" value={email} onChange={handleEmail} />
                            {!isEmailCorrect && email &&  (
                                <p className="label-text-alt link link-hover text-red-500 mt-2">Email not valid</p>
                            )}
                        </div>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Password</span>
                            </label>
                            <input type="password" placeholder="password" className="input input-bordered" onChange={(e) => setPassword(e.target.value)} value={password} />
                            {!isEqualPassword && password &&  (
                                <p className="label-text-alt text-red-500 mt-2">Need To be equal to confirm password</p>
                            )}
                        </div>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Confirm password</span>
                            </label>
                            <input type="password" placeholder="confirm password" className="input input-bordered" value={confirmPassword} onChange={handleConfirmPassword} />
                            {!isEqualPassword && password &&  (
                                <p className="label-text-alt text-red-500 mt-2">Need To be equal to password</p>
                            )}
                        </div>
                        <div className="form-control mt-6">
                            {isEmailCorrect && isEqualPassword && (
                                <button className="btn btn-primary" onClick={submit}>Register</button>
                            ) }
                            {(!isEmailCorrect || !isEqualPassword) && (
                                <button className="btn cursor-default border-red-500 border-2 hover:border-red-500">Register</button>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register;