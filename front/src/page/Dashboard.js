import { useState } from "react";
import classNames from "classnames";
import ArticlesTable from "../components/Dashboard/ArticlesTable";

const Dashboard = () => {
    const [ activeArticlesTab, setActiveArticlesTab ] = useState(true);
    const [ activeUsersTab, setActiveUsersTab ] = useState(false);

    const activeArticles = classNames('tab tab-lg tab-lifted', { 'tab tab-lifted tab-active': activeArticlesTab });
    const activeUsers = classNames('tab tab-lg tab-lifted', { 'tab tab-lifted tab-active': activeUsersTab });

    const handleTabs = (e) => {
        const { text } = e.target;
        if (text === 'Articles') {
            setActiveArticlesTab(true);
            setActiveUsersTab(false);
        } else {
            setActiveArticlesTab(false);
            setActiveUsersTab(true);
        }
    };

    return (
        <div className="m-4 flex flex-col justify-center items-center">
            <h1 className="text-3xl font-bold uppercase mb-4">Bienvenue dans votre dashboard</h1>
            <div className="tabs mb-8">
                <a className={activeArticles} onClick={handleTabs}>Articles</a>
                <a className={activeUsers} onClick={handleTabs}>Utilisateurs</a>
            </div>
            { activeArticlesTab ? (
                    <ArticlesTable />
                ) : (
                    <div>
                        Utilisateurs tab
                    </div>
                )}
        </div>
    )
}

export default Dashboard;