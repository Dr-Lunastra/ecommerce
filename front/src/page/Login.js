import axios from "axios";
import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { validateEmail } from "../utils/validations";
import { UserContext } from "../contexts/userContext";
import { useNavigate } from "react-router-dom";

const Login = () => {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if (user && user.role === 'admin') {
            navigate('/dashboard');
        }
    }, [user, navigate])

    const [ email, setEmail ] = useState('');
    const [ isEmailCorrect, setIsEmailCorrect ] = useState(false);
    const [ password, setPassword ] = useState('');


    const handleEmail = (e) => {
        const { value } = e.target;
        setEmail(value);
        const isValidEmail = validateEmail(value);
        setIsEmailCorrect(isValidEmail);
    }

    const submit = (e) => {
        e.preventDefault();
        if (isEmailCorrect && password) {
            axios.post(`${process.env.REACT_APP_API_URL}users/auth`, {email, password})
            .then((res) => {
                const { token, userData } = res.data;
                localStorage.setItem('token', token);
                setUser(userData);
            })
            .catch((err) => {
                const { data } = err.response;
                displaySwal(data, 'error');
            })
            .finally(() => {
                setEmail('');
                setPassword('');
            })
        }
    }

    const displaySwal = (text, type = 'success') => {
        Swal.fire({
            position: 'top-end',
            icon: type,
            title: text,
            showConfirmButton: false,
            timer: 1500
          })
    }
    return (
        <div className="hero min-h-screen bg-base-200">
            <div className="hero-content flex-col lg:flex-row-reverse">
                <div className="text-center lg:text-left">
                    <h1 className="text-5xl font-bold">Login now!</h1>
                    <p className="py-6">Venez retrouver vos cookies et posez vous !</p>
                </div>
                <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
                    <div className="card-body">
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Email</span>
                            </label>
                            <input type="text" placeholder="email" className="input input-bordered" value={email} onChange={handleEmail}/>
                        </div>
                        <div className="form-control">
                            <label className="label">
                                <span className="label-text">Password</span>
                            </label>
                        <input type="password" placeholder="password" className="input input-bordered" value={password} onChange={(e) => setPassword(e.target.value)}/>
                        </div>
                        <div className="form-control mt-6">
                        {isEmailCorrect && password && (
                            <button className="btn btn-primary" onClick={submit}>Login</button>
                        ) }
                        {(!isEmailCorrect || !password) && (
                            <button className="btn cursor-default border-red-500 border-2 hover:border-red-500">Login</button>
                        )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;