const compareStringEquality = (first, second) => {
    return first === second;
}

export {compareStringEquality}