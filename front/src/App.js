import { Route, Routes } from "react-router-dom";
import Header from "./layout/Header";
import Login from "./page/Login";
import Main from "./page/Main";
import Register from "./page/Register";
import { UserContext } from "./contexts/userContext";
import { useEffect, useState } from "react";
import axios from "axios";
import Dashboard from "./page/Dashboard";

const App = () => {
    const [ user, setUser ] = useState({});

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            axios.get(`${process.env.REACT_APP_API_URL}users/`, {headers: {'Authorization': `Bearer ${token}`}})
            .then((res) => {
                const { user } = res.data;
                setUser(user);
            })
            .catch((err) => {
                console.error(err);
                const { status } = err.response;
                if (status === 401) {
                    localStorage.clear();
                }
            })
        }
    }, []);
 
    return (
        <div>
            <UserContext.Provider value={{ user, setUser }}>
            <Header />
            <Routes>
                <Route path="/" element={<Main />} />
                {user && user.role === 'admin' && (<Route path="/dashboard" element={<Dashboard />} />)}
                <Route path="/login" element={<Login />} />
                <Route path="/register" element={<Register />} />
            </Routes>
            </UserContext.Provider>
        </div>
    )
}

export default App;