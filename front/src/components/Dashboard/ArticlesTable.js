import axios from "axios";
import { useEffect, useState } from "react";
import CreateArticlesModal from "./CreateArticlesModal";

const ArticlesTable = () => {
    const [ articles, setArticles ] = useState([]);
    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}articles/`)
        .then((res) => {
            const { data } = res;
            setArticles(data);
        })
        .catch((err) => {
            console.error(err);
        });
    }, [])

    return (
        <div className="flex flex-col justify-center">
            <label htmlFor="my-modal-3" className="btn w-42 self-start mb-2">Nouveau</label>
            <CreateArticlesModal />
            <div className="overflow-x-auto">
                <table className="table table-zebra w-full">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Photo</th>
                            <th>Prix</th>
                        </tr>
                    </thead>
                    { articles.length > 0 ? (
                    <tbody>
                        <tr>
                            <th>1</th>
                            <td>Cy Ganderton</td>
                            <td>Quality Control Specialist</td>
                            <td>Blue</td>
                        </tr>
                    </tbody>
                    ) : (
                        <tbody>
                        </tbody>
                    ) }
                </table>
            </div>
        </div>
    )
}

export default ArticlesTable;