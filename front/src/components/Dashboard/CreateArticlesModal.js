const CreateArticlesModal = () => {
    return (
        <div>
            <input type="checkbox" id="my-modal-3" className="modal-toggle" />
            <div className="modal">
                <div className="modal-box relative">
                    <label htmlFor="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                    <h3 className="text-lg font-bold">Créer un nouvel article !</h3>
                    <div className="form-control flex flex-col justify-center">
                        <label className="label">
                            <span className="label-text">Nom</span>
                        </label>
                        <label className="input-group">
                            <input type="text" placeholder="Chaussures..." className="input input-bordered" />
                            <span>Nom</span>
                        </label>
                        <label className="label">
                            <span className="label-text">Description</span>
                        </label>
                        <label className="input-group">
                            <input type="text" className="input input-bordered" />
                            <span>Description</span>
                        </label>
                        <label className="label">
                            <span className="label-text">Prix</span>
                        </label>
                        <label className="input-group">
                            <input type="text" placeholder="0.01" className="input input-bordered" />
                            <span>EUR</span>
                        </label>
                        <label className="label">
                            <span className="label-text">Photo</span>
                        </label>
                        <label className="input-group">
                        <input type="file" className="file-input w-full max-w-xs" />
                            <span>Photo</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateArticlesModal;