-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : dim. 15 jan. 2023 à 18:05
-- Version du serveur : 8.0.30
-- Version de PHP : 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `ecommerce`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `statut` varchar(255) DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `activation` tinyint(1) NOT NULL DEFAULT '1',
  `role` varchar(255) NOT NULL DEFAULT 'customer',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `address`, `password`, `activation`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 'hello@hello.com', NULL, '$2b$10$P5ETUVl6kiGaFLwYjZFfiugYsyFa/DTAMPiB.ojJAZ3CwjQu4TTvO', 1, 'customer', '2023-01-06 09:17:47', '2023-01-06 09:17:47'),
(2, 'admin@admin.com', NULL, '$2b$10$PAzIiv1AEhtnPbUnMHYb0OmuDOFnPpyWMPGdaIu4rap6Bh.4HkU6a', 1, 'admin', '2023-01-06 09:18:10', '2023-01-06 09:18:10'),
(4, 'a@a.com', NULL, '$2b$10$VTQhuEsxWoRzuNhMOyQzxOkCzNrIdqdNKSKK2jZSiSZS0/duHcJnK', 1, 'customer', '2023-01-06 17:05:43', '2023-01-06 17:05:43'),
(10, 'b@b.com', NULL, '$2b$10$AvIxe4EFKyeiubnTeanPduTRwif0ZOOmHbPafCF480SgMfjg9EsRK', 1, 'customer', '2023-01-06 17:30:05', '2023-01-06 17:30:05'),
(11, 'jade@gmail.com', NULL, '$2b$10$7LgsQRGFr1y9SBzZUONi5O683Miy124ReH90xA/JsyBFQlLbBJ5r6', 1, 'customer', '2023-01-06 17:57:26', '2023-01-06 17:57:26');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
